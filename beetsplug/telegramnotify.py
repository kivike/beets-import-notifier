from beets.plugins import BeetsPlugin
from beets.util.artresizer import ArtResizer
from shutil import copyfile
import json
import tempfile
import requests

REQ_TIMEOUT_SEC = 30


class TelegramNotify(BeetsPlugin):
    # Wanted art size, image will be downscaled if it's larger
    ART_MAX_SIZE = 200
    # Telegram bot API URL
    API_BASE_URL = "https://api.telegram.org/bot"

    def __init__(self):
        super(TelegramNotify, self).__init__()
        self.register_listener("album_imported", self.album_imported)

    def album_imported(self, lib, album):
        try:
            if not album.artpath:
                print("Missing album art - skipping Telegram notification")
                return
            print(f"Original artpath: {album.artpath}")
            artpath = self.get_resized_art(album.artpath)
            msg = self.format_caption(album)
            self.sendPhotoMessage(artpath, msg)
        except Exception as e:
            print(f"Telegram Notify error: {e}")

    def format_caption(self, album):
        artist = album.albumartist
        album_name = album.album
        genre = album.genre.replace(";", " / ")
        year = album.year
        return f"Album imported\n*{artist} - {album_name}* ({year}) ({genre})"

    def get_resized_art(self, art_file_path):
        """Resize art if the size is too large"""
        size = ArtResizer.shared.get_size(art_file_path)

        if min(size) > self.ART_MAX_SIZE:
            resized_file_path = ArtResizer.shared.resize(
                self.ART_MAX_SIZE, art_file_path
            )
            return resized_file_path
        return art_file_path

    def sendPhotoMessage(self, img_path, msg):
        """Send image with caption to telegram chat
        Returns True if successful, False if error is returned
        """
        bot_token = self.config["bot_token"].get()
        chat_id = self.config["chat_id"].get()
        req_url = self.API_BASE_URL + bot_token + "/sendPhoto"
        with open(img_path, "rb") as img:
            data = {"chat_id": chat_id, "parse_mode": "Markdown", "caption": msg}
            files = {"photo": img}
            try:
                response = requests.post(
                    req_url, data=data, files=files, timeout=REQ_TIMEOUT_SEC
                )
                print("Sent message to Telegram")
                json_data = response.json()
                if "error_code" in json_data:
                    error = json_data["description"]
                    print(f"Error code {json_data['error_code']} returned: {error}")
            except Exception as e:
                print(e)
