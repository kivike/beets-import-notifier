Telegram Beets notifier
==================================


About
-----

Beets integration with Telegram to inform about new albums imported
Based on https://github.com/pintux/whataboutBot

Running the bot
-----
1. Copy userConf.json.sample to userConf.json and fill all values
2. Add the directory to PYTHONPATH
3. Add 'telegramnotify' to plugins in Beets config.yaml

Solving issues
-----
"Plugin telegramnotify not found" when running beet import
- Add directory containing beetsplug directory to PYTHONPATH environmental variable, not the beetsplug directory itself.

Plugin failing to find the album art
- Move telegramnotify plugin to earlier spot in plugins list of beet config file. Other plugins can cause issues with this one.
